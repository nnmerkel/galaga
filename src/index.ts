import Arcade from './cabinet/arcade';

window.addEventListener('DOMContentLoaded', () => {
	const target = document.getElementById('arcade-root');
	const arcade = new Arcade({
		root: target,
	});

	(window as any).arcade = arcade;

	arcade.init();
});

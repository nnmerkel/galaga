import type {
	Ticker,
	Shape,
	Stage as EaselStage,
	Bitmap,
	Sprite,
	Text,
	TickerEvent,
	EventDispatcher,
	EaselEvent,
	DisplayObject,
} from 'easeljs';

// generic types
export interface Dimensions {
	width: number;
	height: number;
}

export interface Coordinates {
	x: number;
	y: number;
}

export type ConsoleMethods =
	| 'warn'
	| 'debug'
	| 'error'
	| 'dir'
	| 'group'
	| 'log';

// application-specific
export type AssetType = 'image' | 'audio' | 'path' | 'font';

export interface AssetUnresolved {
	name: string;
	src: unknown;
	type: AssetType;
}

export interface AssetImage {
	name: string;
	src: HTMLImageElement;
	type: 'image';
}

export interface AssetPath {
	name: string;
	src: HTMLOrSVGImageElement;
	type: 'path';
}

export interface AssetFont {
	name: string;
	src: FontFace;
	type: 'font';
}

export interface AssetAudio {
	name: string;
	src: HTMLAudioElement;
	type: 'audio';
}

export type Asset = AssetImage | AssetPath | AssetFont | AssetAudio;

// custom game objects can be implmented as normal
export interface Cabinet extends Dimensions {
	type: string; // enum
	tile: Dimensions;
}

export interface IAPI {
	protocol: string;
	baseURL: string;
	get<T>(url: string, data?: unknown): Promise<T>; // todo: pass second type arg for payload
	post<T>(url: string, data?: unknown): Promise<T>; // todo: pass second type arg for payload
}

export interface IAudioManager {
	sounds: AssetAudio[];
	soundEnabled: boolean;
	ctx: AudioContext;
	load(sounds: AssetAudio[]): void;
	play(key: string): Promise<void>;
}

export interface IScreen extends Dimensions {
	graphicsScaleFactor: number;
	px: number;
	tile: Dimensions;
	config(targetWidth: number, cabinet: Cabinet): IScreen;
	getUnits(value: number): number;
}

export interface ICreditManager {
	credits: number;
	creditsForPlay: number;
	clear(): void;
	hasCredits(): boolean;
	hasPlay(twoPlayer?: boolean): boolean;
	useCredits(credits: number): boolean;
}

export interface ILogger {
	debug(data: unknown, method?: ConsoleMethods): void;
	error(message: string, data: unknown): void;
}

export interface IPath {
	id: string;
	src: number[];
}

export interface Score {
	name: string;
	score: number;
}

export interface PlayerStatistics {
	hits: number;
	shots: number;
	accuracy: number;
}

export interface IScoreboard {
	scores: number[];
	highScores: Score[];
	load(scores: Score[]): void;
	update(player: number, points: number): void;
	reset(): void;
	getHighScore(place?: number): number;
	setHighScore(name: string, place: number, score: number): void;
}

export interface GridCell extends Coordinates {
	index: number;
}

export interface IUnitFormation extends Coordinates {
	rows: number;
	columns: number;
	direction: number;
	step: number;
	cellWidth: number;
	hSpacing: number;
	vSpacing: number;
	cells: GridCell[];
	getCellX(index: number): number;
	getCellY(index: number): number;
	walk(): void;
	expand(): void;
	updateCells(): void;
	getCoordinates(): Coordinates;
	invertDirection(): void;
	getLeftEdge(): number;
	getRightEdge(): number;
	drawDebugElements(): Shape[];
}

export interface IStage {
	current: number;
	isChallenge: boolean;
	challenges: number;
	pattern: string;
	locations: number[];
	paths: IPath[];
	setPaths(paths: IPath[]): void;
}

export type TemplateFunction<T = void> = (values: Record<string, T>) => string;

// galaga-specific
// objects that inherit from createjs must be classes that extend the createjs class
// todo: this event should already carry data
export type Payload = DisplayObject[] | DisplayObject;

export interface IGameEvent extends EaselEvent {
	data: Payload;
}

export interface FillCommand {
	style: string;
}

export type ClockFunction = (event: TickerEvent) => void;

export interface IClock extends Ticker {
	cycle(frames: number, callback: ClockFunction): ClockFunction;
	timers: { [key: string]: ClockFunction };
	addTimer(name: string, timer: ClockFunction): void;
	clearTimer(name: string): boolean;
}

export interface IJoystick extends EventDispatcher {
	buttons: Set<string>;
	stick: Set<string>;
	eventMap: Map<string, string>;
	keys: Set<string>;
	dispatch: Set<string>;
	stickDispatch: Set<string>;
	setKey(e: KeyboardEvent): void;
	resetKey(e: KeyboardEvent): void;
	read(): void;
}

export interface IStar extends Shape {
	seed: number;
	velocity: number;
	fill: FillCommand; // todo: should be typed a graphics.command type
	update(yCoord: number): void;
}

export interface IMissile extends Bitmap {
	velocity: number;
}

export interface IEntity extends Sprite {
	name: string;
	scoreValue: number;
	multiplier: number;
	health: number;
	behavior: number; // enum
	waveChunk: number;
	formationPosition: number;
	// path: null; // todo: might not need this
	setBehavior(behavior: number /* enum */): void;
	setPosition(x: number, y: number): void;
	getPosition(): Coordinates;
	setMultiplier(multiplier: number): void;
	getScoreValue(): number;
}

export interface IUnitManager extends EventDispatcher {
	units: IEntity[];
	formation: IUnitFormation | null;
	isAssembled: boolean;
	init(boss: AssetImage, goei: AssetImage, zako: AssetImage): void;
	createDefaultFormation(
		boss: AssetImage,
		goei: AssetImage,
		zako: AssetImage
	): IEntity[];
	isFormationCentered(width: number): boolean;
	// todo: update doesnt actually take number[], but locations is typed that way for now
	update(coordinates: number[]): void;
}

export interface ILine extends Text {
	textAlign: string;
}

export interface ITextManager extends EventDispatcher {
	items: Record<string, ILine>;
	fontSize: number;
	lineHeight: number;
	makeLine(text: string, ...rest: unknown[]): ILine; // todo: ...rest should be partial type of a displayobject
	render(view: string, values?: unknown): ILine;
	clear(): void;
}

export interface IPlayer extends Sprite {
	id: number;
	score: number;
	dualFighter: IPlayer | null;
	type: string;
	lives: number;
	state: IStage;
	hitCount: number;
	shotsFired: number;
	awardExtraLife(): void;
	addHit(): void;
	fire(): void;
	handleHit(): void;
	strafeLeft(unit: number): void;
	strafeRight(unit: number): void;
}

// todo: this only needs to pass along options changeable by user control
// all other options should be in IGameOptions to mimic pattern of OG cabinet board switches
export interface IGalagaOptions {
	playerCount?: number;
	playerLives?: number;
	missilesOnScreenLimit?: number;
}

// main game class interface
export interface IGame extends EventDispatcher {
	name: string;
	graphics: EaselStage;
	screen: IScreen;
	turn: number;
	players: IPlayer[];
	loaded: boolean;
	started: boolean;
	paused: boolean;
	isDemo: boolean;
	view: null;
	logger: ILogger;
	clock: IClock;
	scoreboard: IScoreboard;
	audio: IAudioManager;
	textManager: ITextManager;
	unitManager: IUnitManager;
	resources: AssetImage[];
	unitPaths: AssetPath[];
	options: IGalagaOptions;

	// arcade-specific methods
	preload(): Promise<void>;
	init(): void;
	update(tick: TickerEvent): void;
	startDemo(): void;
	endDemo(): void;
	start(options: IGalagaOptions): void;
	enterHighScoreMode(): void;

	// game logic
	addAsset<T extends DisplayObject>(name: string, asset: T): T;
	getResource(name: string): AssetImage;
	getGameUnits(value: number): number;
	getPlayer(id: number): IPlayer | null;
	getActivePlayer(): IPlayer;

	handleAssetCreate(e: IGameEvent): void;
	handleTextAssetCreate(e: IGameEvent): void;
}

export interface IGameOptions {
	name: string;
	canvas: HTMLCanvasElement;
}

export interface IArcadeOptions {
	name?: string;
	root?: HTMLElement | null;
	width?: number;
	rank?: number; // enum
	playerLives?: number;
	playerCount?: number;
	creditsForPlay?: number;
	// shipsPerCredit?
	// playsPerCredit?
}

// main arcade class interface
export interface IArcade {
	options: IArcadeOptions;
	version: string | number;
	cabinet: Cabinet;
	mode: number; // enum
	game: IGame;
	creditManager: ICreditManager;
	joystick: IJoystick;
	logger: ILogger;

	// debug listeners
	// handleButtonA(): void;
	// handleButtonB(): void;
	// handleButtonUp(): void;
	// handleButtonDown(): void;
	// handleButtonLeft(): void;
	// handleButtonRight(): void;
	// handleButtonStart(): void;
	// handleButtonSelect(): void;

	// methods
	run(e: TickerEvent): void;
	setMode(mode: number /* enum */): void;
	resetMode(): void;

	// methods defined by documentation
	runSelfTestMode(): void;
	runAttractMode(): void;
	runReadyMode(): void;
	runPlayMode(): void;
	runHighScoreMode(): void;
}

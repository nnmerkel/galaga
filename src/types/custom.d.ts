// custom typings for resources

// XML-like structures
declare module '*.svg' {
	const content: any;
	export default content;
}

// image formats
declare module '*.png' {
	const content: any;
	export default content;
}

// audio and sound files
declare module '*.mp3' {
	const content: any;
	export default content;
}

// font files
declare module '*.ttf' {
	const content: any;
	export default content;
}

/* eslint-disable import/no-unresolved */

// this is an alias so the typescript compiler will associate all my custom 'easeljs'
// namespace types with the '@createjs/easeljs' js functional code
declare module '@createjs/easeljs' {
	import { EaselEvent } from 'easeljs';
	// todo: eslint cannot see the easeljs namespace/module
	// but everything compiles and ts doesnt complain so idk
	export * from 'easeljs';
	export { EaselEvent as Event };
}

// galaga arcade cabinets have different cabinet configurations
export enum CabinetTypes {
	UPRIGHT = 'UPRIGHT',
	MINI = 'MINI',
	TABLE = 'TABLE',
}

// internal arcade modes
export enum ArcadeMode {
	selftest = -1,
	attract,
	ready,
	play,
	highscore,
}

// A - D, A is easiest, D is hardest
export enum Rank {
	A = 0,
	B,
	C,
	D,
}

export enum Behavior {
	dead = -1, // staged offscreen; health 0. only occurs if players hits unit
	preflight, // staged offscreen, health 1. ready to fly
	inflight, // progressing along preset entrance paths; used for challenge mode as well
	interpolating, // floating from path endpoint to assigned formation coords
	information, // resting in formation
	attacking, // progressing along attacking paths, firing missiles, rolling for transform, rolling for capture formation
	transform, // special move to transform into alternate units
}

import { Shape } from '@createjs/easeljs';
import { boundedRandom, boundedRandomInt } from '~/util';
import type { FillCommand, IStar } from '~/types';

class Star extends Shape implements IStar {
	seed = boundedRandomInt(60, 240);

	velocity = boundedRandom(1, 3);

	fill: FillCommand = { style: '#ffffff' };

	constructor() {
		super();
		this.fill = this.graphics.beginFill('#ffffff').command as FillCommand;
	}

	update(yCoord: number): void {
		this.y = yCoord;
		// mod 360 for hsl, adding 6 is arbitrary
		const newSeed = (this.seed + 6) % 360;
		this.fill.style = `hsl(${newSeed}, 90%, 65%)`;
		this.seed = newSeed;
	}
}

export default Star;

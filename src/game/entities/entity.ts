import { Sprite } from '@createjs/easeljs';
import { Behavior } from '~/enums';
import { defaultLogger } from '../systems/logger';
import type { Coordinates, IEntity } from '~/types';
import type { SpriteSheet } from 'easeljs';

class Entity extends Sprite implements IEntity {
	name = 'default';

	scoreValue = 100;

	multiplier = 1;

	health = 1;

	behavior: Behavior = Behavior.dead;

	waveChunk = 0;

	formationPosition = 0;

	// path = null;

	constructor(spriteSheet: SpriteSheet, name: string, health?: number) {
		super(spriteSheet);
		this.name = name;
		if (typeof health === 'number') this.health = health;
	}

	setBehavior(behavior: Behavior): void {
		if (behavior === this.behavior) {
			return;
		}

		this.behavior = behavior;

		switch (this.behavior) {
			case Behavior.preflight:
				break;
			case Behavior.inflight:
				break;
			case Behavior.interpolating:
				break;
			case Behavior.information:
				break;
			case Behavior.attacking:
				break;
			case Behavior.transform:
				break;
			case Behavior.dead:
				break;
			default:
				defaultLogger.debug(`invalid behavior update: ${behavior}`);
				break;
		}
	}

	setPosition(x: number, y: number): void {
		this.x = x;
		this.y = y;
	}

	getPosition(): Coordinates {
		const { x, y } = this;
		return { x, y };
	}

	// setPath(path) {
	//   this.path = path;
	//   return this;
	// }

	// getPath() {
	//   return this.path;
	// }

	setMultiplier(multiplier: number): void {
		this.multiplier = Math.max(multiplier, 1);
	}

	getScoreValue(): number {
		return this.scoreValue * this.multiplier;
	}
}

export default Entity;

import { Sprite, SpriteSheet } from '@createjs/easeljs';
import Stage from '~/game/systems/stage';
import type { IPlayer, PlayerStatistics } from '~/types';

class Player extends Sprite implements IPlayer {
	// const just to tell player objects apart
	id = 0;

	// individual score
	score = 0;

	// hp
	// todo: why do we need health? dualFighter handles this case
	dualFighter: IPlayer | null = null;

	// from manual
	type = 'GYARAGA';

	// player lives
	lives = 1;

	state = new Stage();

	// stats
	hitCount = 0;

	shotsFired = 0;

	constructor(spriteImage: HTMLImageElement, id: number, lives: number) {
		const playerSpriteData = {
			images: [spriteImage],
			frames: {
				width: 16,
				height: 16,
				margin: 1,
				spacing: 1,
				count: 7,
				regX: 0,
				regY: 0,
			},
			animations: {
				spawn: 6,
			},
		};

		super(new SpriteSheet(playerSpriteData));
		this.id = id;
		this.lives = lives;

		// player stage progress
		// note: this is named 'state' because EaselJS sprites already have a property named 'stage'
		this.state = new Stage();

		this.gotoAndStop('spawn');
	}

	awardExtraLife(): void {
		this.lives += 1;
	}

	addHit(): void {
		this.hitCount += 1;
	}

	fire(): void {
		this.shotsFired += 1;
	}

	handleHit(): void {
		if (this.dualFighter != null) {
			// kill extra fighter, reset player coords and bounds
			this.dualFighter = null;
		} else {
			this.lives -= 1;
		}
	}

	strafeLeft(unit: number): void {
		this.x -= unit;
		if (this.dualFighter != null) {
			this.dualFighter.x -= unit;
		}
	}

	strafeRight(unit: number): void {
		this.x += unit;
		if (this.dualFighter != null) {
			this.dualFighter.x += unit;
		}
	}

	reset(): void {
		this.score = 0;
		// this.health = 1;
		this.shotsFired = 0;
		this.hitCount = 0;
		this.dualFighter = null;
		// this.lives = 0;
	}

	getStatistics(): PlayerStatistics {
		const { shotsFired, hitCount } = this;
		let accuracy;

		if (shotsFired < 1) accuracy = 0;
		else accuracy = ((hitCount / shotsFired) * 100) / 100;

		return {
			hits: hitCount,
			shots: shotsFired,
			accuracy,
		};
	}
}

export default Player;

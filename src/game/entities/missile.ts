import { Bitmap, SpriteSheet } from '@createjs/easeljs';
import type { AssetImage, IMissile } from '~/types';

class Missile extends Bitmap implements IMissile {
	velocity = 2;

	constructor(spriteImage: AssetImage) {
		const data = {
			images: [spriteImage],
		};
		super(new SpriteSheet(data));
	}

	// init() {
	// 	// this.set({
	// 	//   x: this.world.bounds.right + 10, // send offscreen
	// 	//   y: this.world.bounds.bottom,
	// 	//   scaleX: this.world.parent.graphicsScaleFactor,
	// 	//   scaleY: this.world.parent.graphicsScaleFactor,
	// 	//   regY: 8,
	// 	//   regX: 2,
	// 	// });
	// }

	// spawn() {
	// 	// set missile x to be in front of player
	// 	// const { x } = this.world.parent.activePlayer().sprite;
	// 	// this.x = x;
	// }

	// update() {
	// 	// if (this.body.y < this.world.bounds.top) {
	// 	//   this.die();
	// 	// } else {
	// 	//   this.body.y -= Math.floor(this.velocity * this.world.parent.graphicsScaleFactor);
	// 	// }
	// }

	// die() {
	// 	// this.body.set({
	// 	//   x: this.world.bounds.right + 10, // send offscreen
	// 	//   y: this.world.bounds.bottom,
	// 	// });
	// }
}

export default Missile;

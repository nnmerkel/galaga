class Path {
	constructor() {
		// empty
	}
}

export default Path;

// _buildPaths() {
// 	// path order:
// 	// 0: pattern1
// 	// 1: pattern2
// 	// 2: pattern3
// 	// 3-12: challenges 1-10
// 	// paths[n] = [[pathset], ...x13]
// 	// pathset[n] = [[pathdata], ...x5]
// 	// pathdata =
// 	const uniqueWavePatterns = 3;
// 	const uniqueChallengePatterns = 10;
// 	const pathsPerWave = 5;
// 	const paths = {};
// 	const challengePaths = {};
// 	const pathset = [];

// 	this.challengePaths = challengePaths;
// 	this.activePaths = null;

// 	// repeated wave patterns
// 	for (let i = 0; i < uniqueWavePatterns; i++) {
// 		const pathset = [];
// 		const alias = this._patternOrder[i];

// 		for (let j = 0; j < pathsPerWave; j++) {
// 			const path = `${'<path>' + '_'}${alias}_${j}`;
// 			pathset.push(path);
// 		}

// 		paths[alias] = pathset;
// 	}

// 	// challenge wave patterns
// 	for (let i = 0; i < uniqueChallengePatterns; i++) {
// 		const pathset = [];
// 		const alias = `challenge${i + 1}`;

// 		for (let j = 0; j < pathsPerWave; j++) {
// 			const path = `${'<path>' + '_'}${alias}_${j}`;
// 			pathset.push(path);
// 		}

// 		challengePaths[alias] = pathset;
// 	}

// 	// debugger;
// 	const pathString = this.parent.getAsset('path_pattern1_0').body;
// 	const rawPath = MotionPathPlugin.getRawPath(pathString);
// 	MotionPathPlugin.cacheRawPathMeasurements(rawPath);
// 	const p = MotionPathPlugin.getPositionOnPath(rawPath, 0.5, true);

// 	// pathset.push(rawPath);
// 	// paths.push(pathset);
// 	console.log(p);

// 	return paths;
// }

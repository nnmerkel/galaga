import { SpriteSheet } from '@createjs/easeljs';
import Entity from '../entity';
import type { AssetImage } from '~/types';

class Goei extends Entity {
	constructor(spriteImage: AssetImage) {
		const data = {
			images: [spriteImage],
			frames: {
				width: 16,
				height: 16,
				margin: 1,
				spacing: 1,
				count: 7,
				regX: 0,
				regY: 0,
			},
			animations: {
				spawn: 6,
			},
		};

		super(new SpriteSheet(data), 'GOEI', 1);
		this.gotoAndStop('spawn');
	}
}

export default Goei;

import {
	Stage,
	EventDispatcher,
	Shape,
	type TickerEvent,
} from '@createjs/easeljs';
import gsap from 'gsap';
import { MotionPathPlugin } from 'gsap/all';
import { TIMING, EVENTS } from '~/constants';
import {
	resolveAudioAsset,
	resolveFontAsset,
	boundedRandomInt,
	resolveImageAsset,
} from '~/util';
import ScreenProvider from '~/cabinet/screenProvider';
import { getHighScores } from './systems/api/api';
import Scoreboard from './systems/scoreboard';
import AudioManager from './systems/audio/audioManager';
import UnitManager from './systems/unitManager';
import TextManager from './systems/textManager/textManager';
import Player from './entities/player/player';
import Clock from './systems/clock';
import Logger from './systems/logger'; // todo: refactor this into throwing errors? or add error logging
import Star from './entities/star'; // todo: refactor star into a pixel setting, no need to make this an entire shape
import Assets from './assets/assets';
import type {
	AssetAudio,
	Asset,
	AssetImage,
	AssetPath,
	IGame,
	IGameOptions,
	IPlayer,
	IGameEvent,
	IStage,
	IStar,
	IGalagaOptions,
} from '~/types';
import type { DisplayObject } from 'easeljs';

gsap.registerPlugin(MotionPathPlugin);

const delay = (multiplier = 1): Promise<void> => {
	const totalDelay = multiplier * TIMING;
	return new Promise((resolve) => {
		setTimeout(resolve, totalDelay);
	});
};

class Game extends EventDispatcher implements IGame {
	// string name
	name = 'Galaga';

	// easel js graphics instance
	graphics = new Stage(document.createElement('canvas'));

	// screen dimensions, scaling info, and device info
	screen = ScreenProvider();

	// 0 === player 1
	turn = 0;

	players: IPlayer[] = [];

	// game state flags
	loaded = false;

	started = false;

	paused = false;

	isDemo = false;

	stars: IStar[] = []; // todo: do i need to save these here?

	// reference to active text drawn to the screen
	view = null;

	// game subsystems
	// console debugger for dev
	// todo: null this out if not in debug?
	logger = new Logger('Game');

	// animation loop
	clock = new Clock();

	// score handler
	scoreboard = new Scoreboard();

	// sound handler
	audio = new AudioManager();

	// text and static image renderer
	textManager = new TextManager();

	// game brain and unit storage
	unitManager = new UnitManager();

	// external game visual assets
	resources: AssetImage[] = [];

	// full set of unit paths
	unitPaths: AssetPath[] = [];

	// gameplay options
	options: IGalagaOptions = {
		missilesOnScreenLimit: 2,
	};

	constructor({ name, canvas }: IGameOptions) {
		super();

		this.name = name;

		// easel js graphics instance
		this.graphics = new Stage(canvas);

		// text and static image renderer
		// this.textManager.on(
		// 	EVENTS.ASSET.CREATE,
		// 	this.handleTextAssetCreate.bind(this)
		// );

		// game brain and unit storage
		this.unitManager.on(EVENTS.ASSET.CREATE, this.handleAssetCreate.bind(this));

		Game.initialize(this);
	}

	// =============================== ACTION SCRIPT METHODS ==================================

	preload() {
		// display loading state

		// evaluate each type of asset with a processing function
		const sorted: Promise<Asset>[] = Assets.reduce((prev, curr) => {
			const { type } = curr;

			if (type === 'font') {
				prev.push(resolveFontAsset(curr));
			} else if (type === 'audio') {
				prev.push(resolveAudioAsset(curr));
			} else if (type === 'image' || type === 'path') {
				// todo: do paths even need loaded as an image?
				prev.push(resolveImageAsset(curr));
			}

			return prev;
		}, [] as Promise<Asset>[]);

		// todo: refactor these requests, this feels sloppy
		return getHighScores().then((scores) => {
			this.scoreboard.load(scores);

			return Promise.all(sorted)
				.then((results) => {
					console.log('loaded assets:', results);

					const images: AssetImage[] = results.filter(
						(result): result is AssetImage => result.type === 'image'
					);
					this.resources = images;

					const sounds: AssetAudio[] = results.filter(
						(result): result is AssetAudio => result.type === 'audio'
					);
					this.audio.load(sounds);

					const paths: AssetPath[] = results.filter(
						(result): result is AssetPath => result.type === 'path'
					);
					this.unitPaths = paths;

					this.loaded = true;
				})
				.catch((error) => {
					this.logger.error('There was an error loading high scores', error);
				});
		});
	}

	// runs once on arcade startup. initializes graphics objects
	init() {
		if (!this.loaded) {
			this.logger.error('Assets not loaded. Aborting game boot.');
			return;
		}
		const {
			width,
			height,
			tile: { height: tileHeight },
		} = this.screen;
		const { lineHeight } = this.textManager;

		// background
		const background = new Shape();
		background.graphics.beginFill('#000000').drawRect(0, 0, width, height);
		this.addAsset('background', background);

		// stars
		// todo: random spacing has a tendency to cluster. change this to mod
		// or something with i to influence placement and velocity
		// 48 stars is arbitrary
		for (let i = 0; i < 48; i++) {
			const star = new Star();
			star.graphics.drawRect(0, 0, this.getGameUnits(1), this.getGameUnits(1));

			// prevent stars from spawning too close to the edge
			star.x = boundedRandomInt(
				this.getGameUnits(2),
				width - this.getGameUnits(2)
			);
			star.y = tileHeight;

			this.stars.push(star);
			this.addAsset(`star${i}`, star);
		}

		// scoreboard background
		const scoreBackground = new Shape();
		scoreBackground.graphics
			.beginFill('#000000')
			.drawRect(0, 0, width, tileHeight);
		this.addAsset('scoreBackground', scoreBackground);

		// scoreboard elements
		const playerOneScoreHeader = this.textManager.makeLine('1UP', {
			x: width / 6,
			color: '#ff0000',
		});
		const playerOneScoreValue = this.textManager.makeLine(
			`${this.scoreboard.scores[0]}`,
			{
				x: width / 6,
				y: lineHeight,
			}
		);
		const highScoreHeader = this.textManager.makeLine('HIGH SCORE', {
			x: width / 2,
			color: '#ff0000',
		});
		const highScoreValue = this.textManager.makeLine(
			`${this.scoreboard.getHighScore()}`,
			{ x: width / 2, y: lineHeight }
		);
		this.addAsset('playerOneScoreHeader', playerOneScoreHeader);
		this.addAsset('playerOneScoreValue', playerOneScoreValue);
		this.addAsset('highScoreHeader', highScoreHeader);
		this.addAsset('highScoreValue', highScoreValue);

		// todo: these should only appear if two player or if last game was two player
		const playerTwoScoreHeader = this.textManager.makeLine('2UP', {
			x: (5 * width) / 6,
			color: '#ff0000',
		});
		const playerTwoScoreValue = this.textManager.makeLine('0', {
			x: (5 * width) / 6,
			y: lineHeight,
		});
		this.addAsset('playerTwoScoreHeader', playerTwoScoreHeader);
		this.addAsset('playerTwoScoreValue', playerTwoScoreValue);

		// lives and levels background
		const levelsBackground = new Shape();
		levelsBackground.graphics
			.beginFill('#000000')
			.drawRect(0, height - tileHeight, width, height);
		this.addAsset('levelsBackground', levelsBackground);

		// draw assets at offscreen position
		// draw missiles
		// for (let i = 0, n = this.options.missileOnScreenLimit; i < n; i++) {
		//   const missile = new Missile();
		// }

		// todo: pass units to stage to handle?
		this.unitManager.init(
			this.getResource('boss'),
			this.getResource('goei'),
			this.getResource('zako')
		);
	}

	start({ playerCount, playerLives }: IGalagaOptions): void {
		const { src } = this.getResource('playerSprite');
		const {
			width,
			height,
			tile: { height: tileHeight },
			px,
		} = this.screen;
		const { isDemo, turn } = this;

		// load players
		// default playerCount is 1
		for (let i = 0; i < (playerCount || 1); i++) {
			// default playerLives is 3
			const player = new Player(src, i, playerLives || 3);

			player.set({
				x: width / 2,
				y: height - tileHeight - tileHeight,
				scale: px,
			});

			this.players.push(player);
			this.addAsset(`player${i}`, player);
		}

		// handle game events
		if (!isDemo) {
			this.on('button:a', this.handleButtonA);
			this.on('joystick:left', this.handleButtonLeft);
			this.on('joystick:right', this.handleButtonRight);

			// todo: these steps need to be timed. i should build a timing util for this
			// draw lives and stages
			this.renderExtraLives();
			this.renderStageIcons();

			this.audio.enable();
			this.audio.play('stageIntro');

			// display start text
			const readyText = this.textManager
				.render('ready', { player: turn + 1 })
				.set({ x: width / 2, y: height / 2 });
			this.addAsset('readyText', readyText);
		}

		// trigger wave, then game takes over
		delay().then(() => {
			// hide start text
			this.removeAsset('readyText');
			// this.unitManager.allocate(this.getActivePlayer());
			this.clock.addTimer(
				'translateFormation',
				this.clock.cycle(60, this.translateFormation.bind(this))
			);
			this.started = true;
		});
	}

	// main function used by arcade, runs once per frame
	// this function will only be called when this.graphics !== null hence the type assertion
	update(tick: TickerEvent): void {
		// todo: stars dont move when stage is loading or ending
		this.updateStars();

		// if (this.view != null) this.updateTextItems();

		if (this.started) {
			// update units and players
			// const { locations } = this.getActiveStage();
			// this.unitManager.update(locations);
		}

		this.graphics.update(tick);
	}

	end() {
		this.dispose();
		// remove player sprites
		// empty players array
		// reset stages and state
	}

	startDemo() {
		// const { width, height, tile: { height: tileHeight }, px } = this.screen;
		// const demoText = this.textManager.render('test', { copy: 'demo test' }).set({ x: width / 2, y: height / 2 });
		// this.addAsset(demoText, 'demoText');
		this.isDemo = true;
		this.start({
			playerCount: 1,
			playerLives: 1,
		});
	}

	endDemo() {
		this.isDemo = false;
		this.end();
	}

	cycleAttractViews() {
		this.audio.play('stageIntro');
		// cycle between idle views:
		// 1. title screen
		// 2. demo game
		// 3. high scores
	}

	// ============================= SUBROUTINE CALLBACK HANDLERS ================================

	handleAssetCreate(e: IGameEvent): void {
		const { data } = e;
		const singleAsset = !Array.isArray(data);

		if (singleAsset) {
			this.addAsset('singleAsset', data);
			this.logger.debug('single asset: ', data);
		} else {
			data.forEach((entry) => {
				this.addAsset('multipleAssets', entry);
				this.logger.debug('multiple assets', entry);
			});
		}
	}

	handleTextAssetCreate(e: IGameEvent) {
		// const { data } = e;
		this.logger.debug('text asset created: ', e);
	}

	// ============================= ASSET MANAGEMENT ================================

	// todo: needs to handle arrays
	addAsset<T extends DisplayObject>(name: string, asset: T): T {
		asset.set({ name });
		return this.graphics.addChild(asset);
	}

	removeAsset(name: string): boolean {
		const asset = this.graphics.getChildByName(name);
		return this.graphics.removeChild(asset);
	}

	getResource(name: string): AssetImage {
		const resource = this.resources.find((r) => r.name === name);

		if (!resource) {
			this.logger.error(`Asset with name "${name}" was not found`);
			return { name: 'error', src: new Image(), type: 'image' };
		}

		return resource;
	}

	// ================================== PLUMBING METHODS ====================================

	getPlayer(id: number): IPlayer | null {
		return this.players.find((p) => p.id === id) || null;
	}

	getActivePlayer(): IPlayer {
		return this.players[this.turn];
	}

	nextTurn(): number {
		// if (this.players.length < 2) return 0;
		this.turn = (this.turn % 2) + 1;
		return this.turn;
	}

	updateStars(): void {
		const {
			height,
			tile: { height: tileHeight },
		} = this.screen;

		this.stars.forEach((star) => {
			const { y, velocity } = star;
			let newY = 0;
			if (y > height - tileHeight) newY = tileHeight;
			else newY = y + this.getGameUnits(1) * velocity;
			star.update(newY);
		});
	}

	// todo: memoize this? might be worth it for all the calls but if this gets called with arbitrary values the memo could become bloated
	getGameUnits(value: number): number {
		return this.screen.px * value;
	}

	getActiveStage(): IStage {
		const { state } = this.getActivePlayer();
		return state;
	}

	renderExtraLives(): void {
		const { lives } = this.getActivePlayer();
		const {
			height,
			tile: { height: tileHeight },
		} = this.screen;

		for (let i = 0; i < lives; i++) {
			// const badge = new Bitmap('<img>');
			const badge = new Shape();

			badge.graphics
				.beginFill('#ffffff')
				.drawRect(
					i * this.getGameUnits(16),
					height - tileHeight,
					this.getGameUnits(15),
					this.getGameUnits(15)
				);

			this.addAsset(`extraLife${i}`, badge);
		}
	}

	renderStageIcons(): void {
		const { state } = this.getActivePlayer();
		// different badges for every 50, 30, 20, 10, 5, and single stages
		const iconTypes = [50, 30, 20, 10, 5, 1];

		const getIconLayout = (stage: IStage): number[] => {
			let remainder = stage.current;
			let index = 0;
			const ret = [];

			while (index < iconTypes.length) {
				const value = Math.floor(remainder / iconTypes[index]);
				remainder -= iconTypes[index] * value;
				ret.push(value);
				index += 1;
			}

			return ret.reverse();
		};

		const iconCount = getIconLayout(state);
		const {
			width,
			height,
			tile: { height: tileHeight },
		} = this.screen;
		let x = width;

		iconCount.forEach((count, i) => {
			for (let k = 0; k < count; k++) {
				const icon = new Shape();

				x -= this.getGameUnits(16);

				icon.graphics
					.beginFill('#ff0000')
					.drawRect(
						x,
						height - tileHeight,
						this.getGameUnits(15),
						this.getGameUnits(15)
					);

				this.addAsset(`stageIcon${iconTypes.slice().reverse()[i]}`, icon);
			}
		});
	}

	// setActiveView(view) {
	//   this.view = view;
	// }

	handleButtonA() {
		this.getActivePlayer().fire();
		// this.audio.play('fire');
	}

	handleButtonLeft(): void {
		const player = this.getActivePlayer();
		if (player.x <= 0 || !this.started) return;
		player.strafeLeft(this.getGameUnits(1));
	}

	handleButtonRight(): void {
		const player = this.getActivePlayer();
		const {
			width,
			tile: { width: tileWidth },
		} = this.screen;
		if (player.x >= width - tileWidth || !this.started) return;
		player.strafeRight(this.getGameUnits(1));
	}

	// runs once per second, checks units and game state
	// todo: move this to unitmanager
	translateFormation() {
		// update enemy resting coords
		const { width } = this.screen;
		const notEntering =
			this.unitManager.isAssembled &&
			this.unitManager.isFormationCentered(width);

		const rightEdge = this.unitManager.formation.getRightEdge();
		const leftEdge = this.unitManager.formation.getLeftEdge();
		if (rightEdge > width || leftEdge < 0)
			this.unitManager.formation.invertDirection();

		if (notEntering) {
			this.unitManager.formation.expand();
		} else {
			this.unitManager.formation.walk();
		}
	}

	dispose() {
		// reset game, empty buffers, clear stats, reset units, remove listeners
		this.off('button:a', this.handleButtonA);
		this.off('joystick:left', this.handleButtonLeft);
		this.off('joystick:right', this.handleButtonRight);
	}

	enterHighScoreMode(): void {
		// end game, display initials view
		this.textManager.clear();
	}
}

export default Game;

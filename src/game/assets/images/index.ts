import Player from './playerSprite.png';
// import PlayerShip from '../images/playerShip.svg';
import StageOne from './stageOne.svg';
import StageFive from './stageFive.svg';
import StageTen from './stageTen.svg';
import StageTwenty from './stageTwenty.svg';
import StageThirty from './stageThirty.svg';
import StageFifty from './stageFifty.svg';
import Galaga from './galagaLogo.svg';
import ExtraLife from './playerLife.svg';
import PlayerMissile from './playerMissile.svg';
import ZakoSprite from './zakoSprite.svg';
import GoeiSprite from './goeiSprite.svg';
import BossSprite from './bossSprite.svg';
import type { AssetImage } from '~/types';

const images: AssetImage[] = [
	{
		name: 'playerSprite',
		src: Player,
		type: 'image',
	},
	{
		name: 'stageOneBadge',
		src: StageOne,
		type: 'image',
	},
	{
		name: 'stageFiveBadge',
		src: StageFive,
		type: 'image',
	},
	{
		name: 'stageTenBadge',
		src: StageTen,
		type: 'image',
	},
	{
		name: 'stageTwentyBadge',
		src: StageTwenty,
		type: 'image',
	},
	{
		name: 'stageThirtyBadge',
		src: StageThirty,
		type: 'image',
	},
	{
		name: 'stageFiftyBadge',
		src: StageFifty,
		type: 'image',
	},
	{
		name: 'galagaLogo',
		src: Galaga,
		type: 'image',
	},
	{
		name: 'extraLife',
		src: ExtraLife,
		type: 'image',
	},
	{
		name: 'playerMissile',
		src: PlayerMissile,
		type: 'image',
	},
	{
		name: 'zako',
		src: ZakoSprite,
		type: 'image',
	},
	{
		name: 'goei',
		src: GoeiSprite,
		type: 'image',
	},
	{
		name: 'boss',
		src: BossSprite,
		type: 'image',
	},
];

export default images;

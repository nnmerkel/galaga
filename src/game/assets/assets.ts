// todo: refactor this file similarly to i18n collection
// instead of manually listing assets, should we import all contents within a file?
// all contents matching a naming convention?
// is manual better in this instance?

import images from './images';
import paths from './paths';
import fonts from './fonts';
import audio from './audio';
import type { Asset } from '~/types';

const assets: Asset[] = [...images, ...paths, ...fonts, ...audio];

export default assets;

import pressStart2P from './PressStart2P.ttf';
import type { AssetFont } from '~/types';

const fonts: AssetFont[] = [
	{
		name: 'PressStart2P',
		src: pressStart2P,
		type: 'font',
	},
];

export default fonts;

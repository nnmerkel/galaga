import examplePath from './examplePath_1.svg';
import type { AssetPath } from '~/types';

// name syntax: 'path_pattern + <pattern type> + _ + <pattern wave chunk>'

const paths: AssetPath[] = [
	{
		name: 'path_pattern1_0',
		src: examplePath,
		type: 'path',
	},
];

export default paths;

import stageIntro from './01 Stage Intro.mp3';
import fighterCaptured from './02 Fighter Captured.mp3';
import fighterRescued from './03 Fighter Rescued.mp3';
import capturedFighterDestroyed from './04 Captured Fighter Destroyed.mp3';
import challengingStage from './05 Challenging Stage.mp3';
import challengingStageOver from './06 Challenging Stage Over.mp3';
import challengingStagePerfect from './07 Challenging Stage Perfect.mp3';
import oneUp from './08 1-Up.mp3';
import fighterDestroyed from './09 Die-Start Up Sound.mp3';
import coin from './10 Coin.mp3';
import nameEntry from './11 Name Entry.mp3';
import unknown from './12 Unknown.mp3';
import soundEffects from './13 Sound Effects.mp3';
import type { AssetAudio } from '~/types';

const sounds: AssetAudio[] = [
	{
		name: 'stageIntro',
		src: stageIntro,
		type: 'audio',
	},
	{
		name: 'fighterCaptured',
		src: fighterCaptured,
		type: 'audio',
	},
	{
		name: 'fighterRescued',
		src: fighterRescued,
		type: 'audio',
	},
	{
		name: 'capturedFighterDestroyed',
		src: capturedFighterDestroyed,
		type: 'audio',
	},
	{
		name: 'challengingStage',
		src: challengingStage,
		type: 'audio',
	},
	{
		name: 'challengingStageOver',
		src: challengingStageOver,
		type: 'audio',
	},
	{
		name: 'challengingStagePerfect',
		src: challengingStagePerfect,
		type: 'audio',
	},
	{
		name: 'oneUp',
		src: oneUp,
		type: 'audio',
	},
	{
		name: 'fighterDestroyed',
		src: fighterDestroyed,
		type: 'audio',
	},
	{
		name: 'coin',
		src: coin,
		type: 'audio',
	},
	{
		name: 'nameEntry',
		src: nameEntry,
		type: 'audio',
	},
	{
		name: 'unknown',
		src: unknown,
		type: 'audio',
	},
	{
		name: 'soundEffects',
		src: soundEffects,
		type: 'audio',
	},
];

export default sounds;

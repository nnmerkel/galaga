import type { TemplateFunction } from '~/types';

// list of all possible text views
// multi-line
const challengeEnd: TemplateFunction<number> = ({
	numberOfHits,
	bonus,
}): string => `Number of Hits ${numberOfHits}
Bonus ${bonus}`;

const perfectChallengeEnd: TemplateFunction = (): string => `Perfect
Number of Hits 40
Special Bonus 10000`;

const results: TemplateFunction<number> = ({
	shotsFired,
	hits,
	accuracy,
}): string => `Results
Shots Fired ${shotsFired}
Number of Hits ${hits}
Hit-Miss Ratio ${accuracy}%`;

const highScore: TemplateFunction<string | number> = ({
	firstInitial,
	secondInitial,
	thirdInitial,
	score,
}): string => `Enter Your Initials
Name ${firstInitial}${secondInitial}${thirdInitial}
Score ${score}`;

const dynamicTopFive: TemplateFunction<string | number> = ({
	firstInitial,
	secondInitial,
	thirdInitial,
	newScore,
	scores,
}): string =>
	`${firstInitial} ${secondInitial} ${thirdInitial} ${newScore} ${scores}`;

const staticTopFive: TemplateFunction<number[]> = ({ scores }): string =>
	`${scores}`;

const templates: Record<string, TemplateFunction<any>> = {
	test: ({ copy }) => `test: ${copy}`,
	selfTest: () => 'Self Test',
	gameStart: () => 'Ready',
	player: ({ player }) => `Player ${player}`,
	stage: ({ stage }) => `Stage ${stage}`,
	challenge: () => 'Challenging Stage',
	challengeEnd,
	perfectChallengeEnd,
	ready: ({ player }) => `Player ${player} - Ready`,
	readyStage: ({ player, stage }) => `Player ${player} - Stage ${stage}`,
	gameOver: () => 'Game Over',
	results,
	highScore,
	dynamicTopFive,
	staticTopFive,
};

export default templates;

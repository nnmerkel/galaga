import { EventDispatcher } from '@createjs/easeljs';
import { EVENTS } from '~/constants';
import ScreenProvider from '~/cabinet/screenProvider';
import GameEvent from '../gameEvent';
import templates from './templates';
import Line from './line';
import type { ILine, ITextManager } from '~/types';

class TextManager extends EventDispatcher implements ITextManager {
	items: Record<string, ILine> = {};

	fontSize = 7;

	lineHeight = 8;

	constructor() {
		super();
		this.fontSize = ScreenProvider().getUnits(7);
		this.lineHeight = ScreenProvider().getUnits(8);

		TextManager.initialize(this);
	}

	makeLine(text: string, ...rest: any[]): ILine {
		return new Line(text, this.fontSize, '#ffffff', ...rest);
	}

	render(view: string, values?: any): ILine {
		const line = this.makeLine(templates[view](values));
		const e = new GameEvent(EVENTS.ASSET.CREATE, line);

		this.items[view] = line;
		this.dispatchEvent(e);

		return line;
	}

	// renderTable(columns, rows) {
	// 	console.log();
	// }

	// setActiveView(item, customTiming) {
	//   this.active = item;

	//   setTimeout(() => {
	//     this.active = null;
	//   }, customTiming);

	//   return this.active;
	// }

	clear(): void {
		// this.items.length = 0;
		this.items = {};
	}
}

export default TextManager;

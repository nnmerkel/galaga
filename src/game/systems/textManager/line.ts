import { Text } from '@createjs/easeljs';
import type { ILine } from '~/types';

class Line extends Text implements ILine {
	constructor(text: string, fontSize: number, color: string, ...rest: any[]) {
		// todo: make font name dynamic so it can be customized
		super(text, `${fontSize}px PressStart2P`, color || '#ffffff');

		this.textAlign = 'center';
		Object.assign(this, ...rest);
	}
}

export default Line;

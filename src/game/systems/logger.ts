import { DEBUG } from '~/constants';
import type { ConsoleMethods, ILogger } from '~/types';

class Logger implements ILogger {
	moduleName = 'Default';

	constructor(moduleName?: string) {
		if (moduleName) this.moduleName = moduleName;
	}

	debug(message: string, data?: unknown, method?: ConsoleMethods): void {
		if (!DEBUG) return;
		const fullMessage = data ? `${message} ${data}` : `${message}`;
		// eslint-disable-next-line no-console
		console[method || 'log'](`[${this.moduleName}] `, fullMessage);
	}

	error(message: string, data?: unknown): void {
		const fullMessage = data ? `${message} ${data}` : `${message}`;
		// eslint-disable-next-line no-console
		console.error(`[${this.moduleName}] `, fullMessage);
	}
}

const defaultLogger = new Logger();

export { defaultLogger };

export default Logger;

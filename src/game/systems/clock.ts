import { Ticker } from '@createjs/easeljs';
import { defaultLogger } from './logger';
import type { IClock, ClockFunction } from '~/types';
import type { TickerEvent } from 'easeljs';

class Clock extends Ticker implements IClock {
	timers: { [key: string]: ClockFunction } = {};

	constructor() {
		super();
		Clock.framerate = 60;
		Clock.timingMode = 'raf'; // use requestAnimationFrame
	}

	addTimer(name: string, timer: ClockFunction) {
		this.clearTimer(name);
		this.timers[name] = timer;
	}

	// hook into the game clock to keep interval functions synched
	// eslint-disable-next-line class-methods-use-this
	cycle(frames: number, callback: ClockFunction): ClockFunction {
		const start = Clock.getTicks(true);

		const interval = Clock.on('tick', (e: TickerEvent) => {
			const ticks = Clock.getTicks(true);

			if ((ticks - start) % frames === 0) {
				callback(e);
			}
		});

		return interval;
	}

	clearTimer(name: string): boolean {
		const timer = this.timers[name];
		if (!timer) return false;

		Clock.off('tick', timer);
		// prune off removed timer in a ts-safe way; removedTimer is the unused deleted timer reference
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		const { [name]: removedTimer, ...rest } = this.timers;
		this.timers = rest;

		const didDelete = delete this.timers[name];

		if (!didDelete) {
			defaultLogger.debug(`timer "${name}" failed to delete`);
			return false;
		}

		return true;
	}
}

export default Clock;

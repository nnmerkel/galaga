import { Shape } from '@createjs/easeljs';
import ScreenProvider from '~/cabinet/screenProvider';
import { DEBUG } from '~/constants';
import type { Coordinates, GridCell, IUnitFormation, IEntity } from '~/types';
import type { Shape as EaselShape } from 'easeljs';

interface GridCellDebug extends GridCell {
	contents: Shape;
}

class UnitFormation implements IUnitFormation {
	rows = 5;

	columns = 10;

	x = 0;

	y = 0;

	direction = 1;

	step = 1;

	cellWidth = 1;

	hSpacing = 1;

	vSpacing = 1;

	cells: GridCell[] = [];

	// todo: no need to pass waveChunk here
	constructor(template: Pick<IEntity, 'waveChunk' | 'formationPosition'>[]) {
		const {
			width,
			tile: { height: tileHeight },
		} = ScreenProvider();

		this.x = width / 2;
		this.y = tileHeight;
		this.step = ScreenProvider().getUnits(8);
		this.cellWidth = ScreenProvider().getUnits(16);
		this.hSpacing = ScreenProvider().getUnits(2);
		this.vSpacing = ScreenProvider().getUnits(4);
		this.cells = template.map(
			(unit: Pick<IEntity, 'waveChunk' | 'formationPosition'>) => ({
				index: unit.formationPosition,
				x: 0,
				y: 0,
			})
		);
	}

	drawDebugElements(): EaselShape[] {
		const boxes: EaselShape[] = [];

		this.cells.forEach((cell) => {
			const box = new Shape();
			let color = '#ffff00';
			if (cell.index < 9) color = '#0000ff';
			else if (cell.index > 10 && cell.index < 29) color = '#ff0000';

			box.graphics.s(color).r(0, 0, this.cellWidth, this.cellWidth);
			boxes.push(box);
			// eslint-disable-next-line no-param-reassign
			(cell as GridCellDebug).contents = box;
		});

		return boxes;
	}

	getCellX(index: number): number {
		const finalDigit = index % 10;
		const symmetryAxis = this.columns / 2;
		const cellXOffset =
			finalDigit < symmetryAxis
				? (symmetryAxis - finalDigit) * (-this.cellWidth - this.hSpacing)
				: (finalDigit - symmetryAxis) * (this.cellWidth + this.hSpacing);

		return this.x + cellXOffset;
	}

	getCellY(index: number): number {
		const row = Math.floor(index / this.columns);
		return this.y + row * this.vSpacing + row * this.cellWidth;
	}

	walk(): void {
		// const leftEdge = this.getCellX(this.cells.length - this.columns);
		// const rightEdge = this.getCellX(this.cells.length - 1) + this.cellWidth;

		// if (leftEdge < 0 || rightEdge > width) {
		//   this.direction *= -1;
		// }

		this.x += this.step * this.direction;
		this.updateCells();
	}

	// todo: the way this expands ensures that column 6 units are always in the same x position
	// this needs refactored so that unit x positions are mirrored across the y-axis
	// that could also save on time. calculate one side of x-coords and then mirror
	expand(): void {
		// const leftEdge = this.getCellX(this.cells.length - this.columns);
		// const rightEdge = this.getCellX(this.cells.length - 1) + this.cellWidth;

		// if (leftEdge < 0 || rightEdge > width) {
		//   this.direction *= -1;
		// }

		const result = (this.step / 4) * this.direction;
		this.hSpacing += result;

		if (this.hSpacing <= 0) {
			this.hSpacing = 0;
			this.direction *= -1;
		}

		this.updateCells();
	}

	updateCells(): void {
		for (let i = 0, n = this.cells.length; i < n; i++) {
			const current = this.cells[i];

			const newX = this.getCellX(current.index);
			const newY = this.getCellY(current.index);

			// debugging only
			if (DEBUG) {
				(current as GridCellDebug).contents.x = newX;
				(current as GridCellDebug).contents.y = newY;
			}

			// if the unit is flying around, let the game world handle the update
			// if (!current.isInFormation) continue;
			current.x = newX;
			current.y = newY;
		}
	}

	getCoordinates(): Coordinates {
		const { x, y } = this;
		return { x, y };
	}

	invertDirection(): void {
		this.direction *= -1;
	}

	getLeftEdge(): number {
		return this.getCellX(this.cells.length - this.columns);
	}

	getRightEdge(): number {
		return this.getCellX(this.cells.length - 1) + this.cellWidth;
	}
}

export default UnitFormation;

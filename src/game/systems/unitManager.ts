import { EventDispatcher } from '@createjs/easeljs';
import { Behavior } from '~/enums';
import { DEBUG, EVENTS } from '~/constants';
import Boss from '../entities/ai/boss';
import Goei from '../entities/ai/goei';
import Zako from '../entities/ai/zako';
import GameEvent from './gameEvent';
import UnitFormation from './unitFormation';
import type { AssetImage, IUnitManager, IEntity } from '~/types';

// todo: nomenclature. is it a formation or a convoy?

// 0, 1, 2, 7, 8, 9, 10, 19, 20, 29 are empty
// position of each number is physical position on screen
// value corresponds to order in which fighters enter screen
// prettier-ignore
const formationTemplate = [
	0, 0, 0, 2, 2, 2, 2, 0, 0, 0,
	0, 3, 3, 2, 1, 1, 2, 3, 3, 0,
	0, 3, 3, 2, 1, 1, 2, 3, 3, 0,
	5, 5, 4, 4, 1, 1, 4, 4, 5, 5,
	5, 5, 4, 4, 1, 1, 4, 4, 5, 5,
];

const unitTemplate: Pick<IEntity, 'waveChunk' | 'formationPosition'>[] =
	formationTemplate.map((value, index) => {
		return { waveChunk: value, formationPosition: index };
	});

// this class is designed to be a handler for the units in game state.
// it should not store its own units, but rather generate and update units
// this class should also hold methods for picking units to attack and handling behavioral updates
class UnitManager extends EventDispatcher implements IUnitManager {
	// holds refs to all units in memory. these units will be updated when they are passed back via Stage
	units: IEntity[] = [];

	// holds refs to all alive units on game field/units that need redrawn
	// this.updateableUnits = [];

	// holds refs to transformed special units
	// this.transforms = [];

	// holds resting/idle x:y coords for enemy units
	formation = new UnitFormation(unitTemplate);

	// holds coord data for enemy unit paths
	// this.paths = [];

	// holds enemy units divided into buckets based on entrance pattern
	// currently, this only supports 5 chunks, no more no less
	// this.chunks = [];

	isAssembled = false;

	constructor() {
		super();
		UnitManager.initialize(this);
	}

	init(boss: AssetImage, goei: AssetImage, zako: AssetImage): void {
		const formation: IEntity[] = this.createDefaultFormation(boss, goei, zako);

		this.formation = new UnitFormation(formation);
		this.units = formation;

		if (DEBUG) {
			const outlines = this.formation.drawDebugElements();
			this.dispatchEvent(new GameEvent(EVENTS.ASSET.CREATE, outlines));
		}
	}

	createDefaultFormation(
		boss: AssetImage,
		goei: AssetImage,
		zako: AssetImage
	): IEntity[] {
		const formation = formationTemplate
			.map((value, i) => {
				let unit: IEntity | null = null;
				const empty = value === 0;

				if (i < 10 && !empty) unit = new Boss(boss);
				else if (i < 30 && i > 9 && !empty) unit = new Goei(goei);
				/* (i >= 30 && !empty) */ else unit = new Zako(zako);

				unit.set({
					formationPosition: i,
					waveChunk: value,
				});

				return unit;
			})
			.filter((value) => value.waveChunk !== 0);

		this.dispatchEvent(new GameEvent(EVENTS.ASSET.CREATE, formation));

		return formation;
	}

	// allocate(player) {
	// grab a set of rendered units for use by the active player
	// or
	// grab a set of stored unit coords and statuses from the player and assign them to the units on the board
	// }

	isFormationCentered(width: number): boolean {
		const { x } = this.formation.getCoordinates();
		return x === width / 2;
	}

	// start() {
	// build wave and formation
	// align units to initial positions offscreen
	// trigger flight behavior
	// }

	update(coordinates: number[]): void {
		// main update loop here?
		// update active units
		// run transforms from here? or from index?
		const toUpdate = this.units.filter((u) => u.behavior !== Behavior.dead);
		console.log(coordinates, toUpdate);
	}

	// clear() {
	// 	this.paths.length = 0;
	// 	this.chunks.length = 0;
	// }

	// dispose() {}
}

export default UnitManager;

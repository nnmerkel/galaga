import { Event } from '@createjs/easeljs';
import type { Payload, IGameEvent } from '~/types';

class GameEvent<T extends Payload> extends Event implements IGameEvent {
	constructor(
		type: string,
		payload: T,
		bubbles?: boolean,
		cancelable?: boolean
	) {
		super(
			type,
			bubbles != null ? bubbles : true,
			cancelable != null ? cancelable : true
		);

		if (payload) this.data = payload;
	}
}

export default GameEvent;

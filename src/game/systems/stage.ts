import type { IPath, IStage } from '~/types';

// list of all possible flight patterns
const patterns = ['pattern1', 'pattern2', 'pattern3', 'challenge'];

// this class should represent a full implementation of galaga game state for a single player
// what is the difference between this and unitmanager?
class Stage implements IStage {
	// reference to the players stage number, defaults to 1
	current = 1;

	// flag indicating if this level is a challenge stage or not
	isChallenge = false;

	// total number of challenges played
	challenges = 0;

	// string indicating which flight pattern should be used for this level
	pattern = patterns[0];

	// the initial positions of active units
	locations: number[] = []; // todo: this isnt number[]

	// the set of paths that active units will use to interpolate position
	paths: IPath[] = [];

	constructor(start = 1) {
		this.current = start;

		// in case we passed in a custom start level, config here
		this.setChallenge();
		this.setPattern();
		// this.setPaths();
	}

	advance() {
		this.current += 1;
		this.setChallenge();
		this.setPattern();
		return this.current;
	}

	setChallenge() {
		// decide if its a challenge stage or not
		// challenge stages start at 3 and repeat every 4th level, after each pattern type executes
		// 3, 7, 11, 15, etc
		this.isChallenge = (this.current + 1) % 4 === 0;

		// todo: mod this off by the number of challenge stage patterns
		if (this.isChallenge) this.challenges += 1;
	}

	setPattern() {
		// set entrance pattern, 3rd pattern is skipped first time around
		// stage 1 = pattern1
		// stage 2 = pattern2
		// stage 3 = challenge0
		// stage 4 = pattern1
		// stage 5 = pattern2
		// stage 6 = pattern3
		// stage 7 = challenge1
		// repeat 4-7
		const { current } = this;

		if (current < 4) {
			if (current === 3) {
				this.pattern = patterns[current];
			} else {
				this.pattern = patterns[current - 1];
			}
		} else {
			this.pattern = patterns[current % 4];
		}
	}

	setPaths(paths: IPath[]): void {
		this.paths = paths;
	}

	reset() {
		this.challenges = 0;
		this.current = 1;
		this.locations = [];
		this.setPattern();
		this.setChallenge();
	}
}

export default Stage;

// todo: implement PAPU system from NES?
import type { AssetAudio, IAudioManager } from '~/types';

class AudioManager implements IAudioManager {
	sounds: AssetAudio[] = [];

	soundEnabled = false;

	ctx: AudioContext = new AudioContext();

	constructor() {
		// empty
	}

	disable() {
		this.soundEnabled = false;
	}

	enable() {
		this.ctx.resume();
		this.soundEnabled = true;
	}

	load(sounds: AssetAudio[]): void {
		this.sounds = sounds;
	}

	play(key: string): Promise<void> {
		if (!this.soundEnabled) return Promise.resolve();

		const toPlay = this.sounds.find((sound) => sound.name === key);

		if (!toPlay) {
			throw new Error(`[AudioManager | play]: no sound found with key ${key}`);
		}

		return toPlay.src.play();
	}
}

export default AudioManager;

import type { IAPI, Score } from '~/types';

const API_BASE = `${process.env.API_BASE}`;
const API_NAMESPACE = `${process.env.API_NAMESPACE}`;

const Endpoints = {
	scores: '/highscores',
	updateScores: '',
};

class API implements IAPI {
	protocol = 'http';

	baseURL = '';

	constructor(protocol?: string) {
		this.protocol = protocol || 'http';
		this.baseURL = `${this.protocol}://${API_BASE}/${API_NAMESPACE}`;
	}

	get<T>(url: string, data?: unknown): Promise<T> {
		return fetch(this.baseURL + url, {
			method: 'GET',
			body: JSON.stringify(data),
		}).then((response) => response.json());
	}

	post<T>(url: string, data?: unknown): Promise<T> {
		return fetch(this.baseURL + url, {
			method: 'POST',
			body: JSON.stringify(data),
		}).then((response) => response.json());
	}
}

const client = new API();

const getHighScores = () => {
	return client.get<Score[]>(Endpoints.scores);
};

const updateHighScores = (name: string, place: number, score: number) => {
	return client.post<Score[]>(Endpoints.updateScores, { name, place, score });
};

export { getHighScores, updateHighScores };

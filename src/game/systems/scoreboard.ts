import { updateHighScores } from './api/api';
import type { IScoreboard, Score } from '~/types';

class Scoreboard implements IScoreboard {
	scores = [0, 0];

	highScores: Score[] = [];

	constructor() {
		// empty
	}

	load(scores: Score[]) {
		this.highScores = scores;
	}

	update(player: number, points: number): void {
		this.scores[player] += points;
	}

	reset(): void {
		this.scores = [0, 0];
	}

	getHighScore(place?: number): number {
		const entry = this.highScores[(place == null ? 1 : place) - 1];
		return entry && entry.score;
	}

	setHighScore(name: string, place: number, score: number): Promise<Score[]> {
		return updateHighScores(name, place, score)
			.then((newScores) => {
				this.highScores = newScores;
				return Promise.resolve(newScores);
			})
			.catch((error) => {
				return Promise.reject(error);
			});
	}
}

export default Scoreboard;

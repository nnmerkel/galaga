import type {
	AssetAudio,
	AssetFont,
	AssetImage,
	AssetUnresolved,
} from './types';

export const boundedRandomInt = (min: number, max: number): number => {
	const boundMin = Math.ceil(min);
	const boundMax = Math.floor(max);
	return Math.floor(Math.random() * (boundMax - boundMin) + boundMin);
};

export const boundedRandom = (min: number, max: number): number => {
	return Math.random() * (max - min) + min;
};

export const constgetBezierXY = (
	t: number,
	sx: number,
	sy: number,
	cp1x: number,
	cp1y: number,
	cp2x: number,
	cp2y: number,
	ex: number,
	ey: number
): { x: number; y: number } => {
	return {
		x:
			(1 - t) ** 3 * sx +
			3 * t * (1 - t) ** 2 * cp1x +
			3 * t * t * (1 - t) * cp2x +
			t * t * t * ex,
		y:
			(1 - t) ** 3 * sy +
			3 * t * (1 - t) ** 2 * cp1y +
			3 * t * t * (1 - t) * cp2y +
			t * t * t * ey,
	};
};

export const getBezierAngle = (
	t: number,
	sx: number,
	sy: number,
	cp1x: number,
	cp1y: number,
	cp2x: number,
	cp2y: number,
	ex: number,
	ey: number
): number => {
	const dx =
		(1 - t) ** 2 * (cp1x - sx) +
		2 * t * (1 - t) * (cp2x - cp1x) +
		t * t * (ex - cp2x);
	const dy =
		(1 - t) ** 2 * (cp1y - sy) +
		2 * t * (1 - t) * (cp2y - cp1y) +
		t * t * (ey - cp2y);
	return -Math.atan2(dx, dy) + 0.5 * Math.PI;
};

export const resolveImageAsset = (
	asset: AssetUnresolved
): Promise<AssetImage> => {
	const { src } = asset;

	return new Promise((resolve, reject) => {
		const i = new Image();
		const resolvedAsset: AssetImage = {
			name: asset.name,
			src: i,
			type: 'image',
		};

		i.onload = () => resolve(resolvedAsset);
		i.onerror = () => reject();

		i.src = `${src}`;
	});
};

export const resolveAudioAsset = (
	asset: AssetUnresolved
): Promise<AssetAudio> => {
	const { src } = asset;
	const sound = new Audio(`${src}`);
	const resolvedAsset: AssetAudio = {
		name: asset.name,
		src: sound,
		type: 'audio',
	};

	return Promise.resolve(resolvedAsset);
};

export const resolveFontAsset = (
	asset: AssetUnresolved
): Promise<AssetFont> => {
	const { name, src } = asset;
	const font = new FontFace(name, `url('${src}')`);

	return font.load().then((loaded) => {
		const resolvedAsset: AssetFont = {
			name: asset.name,
			src: loaded,
			type: 'font',
		};
		document.fonts.add(loaded);
		return resolvedAsset;
	});
};

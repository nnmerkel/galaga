export const VERSION = '1.0.0';
export const KEYBINDS = {
	a: ' ',
	b: 'c',
	start: 'z',
	select: 'x',
	print: 'p',
};
export const JOYSTICK = {
	up: 'ArrowUp',
	down: 'ArrowDown',
	left: 'ArrowLeft',
	right: 'ArrowRight',
};
export const EVENTS = {
	ASSET: {
		CREATE: 'asset:create',
	},
};
export const TIMING = 5000;
export const DEBUG = process.env.NODE_ENV !== 'production';
export const DEFAULT_WIDTH = 240;
export const DEFAULT_HEIGHT = 336;
export const DEFAULT_TILE = 16;

// todo: build this later. it has no purpose in the browser but maybe it can be useful
import { defaultLogger } from '~/game/systems/logger';
import type { ICreditManager } from '~/types';

class CreditManager implements ICreditManager {
	credits = 0;

	tokensPerCredit = 1;

	creditsForPlay = 2;

	creditsForTwoPlay = 4;

	constructor(creditsForPlay: number) {
		// todo: see switchboard configurations to set up this conversion
		this.creditsForPlay = creditsForPlay;
		this.creditsForTwoPlay = this.creditsForPlay * 2;
	}

	clear(): void {
		this.credits = 0;
	}

	hasCredits(): boolean {
		return this.credits > 0;
	}

	hasPlay(twoPlayer?: boolean): boolean {
		const { creditsForPlay, creditsForTwoPlay, credits } = this;
		const threshold = twoPlayer ? creditsForTwoPlay : creditsForPlay;
		return credits >= threshold;
	}

	useCredits(credits: number): boolean {
		if (credits < 0) {
			defaultLogger.debug('[CreditManager]: cannot use negative credits');
			return false;
		}

		if (credits > this.credits) this.credits = 0;
		else this.credits -= credits;
		return true;
	}
}

export default CreditManager;

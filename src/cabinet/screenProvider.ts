import { DEFAULT_HEIGHT, DEFAULT_TILE, DEFAULT_WIDTH } from '~/constants';
import type { IScreen } from '~/types';

const internalScreen: IScreen = {
	width: DEFAULT_WIDTH,
	height: DEFAULT_HEIGHT,
	graphicsScaleFactor: 1,
	px: 1,
	tile: {
		width: DEFAULT_TILE,
		height: DEFAULT_TILE,
	},
	config: function config(targetWidth, cabinet) {
		const { width, height, tile } = cabinet;

		// this SHOULD always be 5:7
		const aspectRatio = width / height;

		// limit smallest width to default arcade width. calculations cannot run when game dimensions are
		// less then 1 pixel per game unit
		this.width = Math.max(Math.floor(targetWidth), DEFAULT_WIDTH);
		this.height = Math.floor(this.width / aspectRatio);

		// this is the width of a "Galaga pixel" in terms of screen/css pixels.
		// this is the smallest unit of length possible in our "scanline" world
		// all assets will be scaled according to this factor when drawn to the game world

		// todo: round everything in here so it lines up pixel for pixel
		this.graphicsScaleFactor = targetWidth / width;
		this.px = this.graphicsScaleFactor;

		this.tile = {
			width: tile.width * this.px,
			height: tile.height * this.px,
		};

		return this;
	},
	getUnits: function getUnits(value) {
		return Math.floor(value * this.px);
	},
};

// todo: rewrite prop assignment?
const provider = (screen: Partial<IScreen>): IScreen =>
	Object.assign(internalScreen, screen);

export default function ScreenProvider(options?: Partial<IScreen>) {
	return provider(options || {});
}

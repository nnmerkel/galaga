import { EventDispatcher } from '@createjs/easeljs';
import { KEYBINDS, JOYSTICK } from '~/constants';
import type { IJoystick } from '~/types';

const joystickPrefix = 'joystick';
const keyboardPrefix = 'button';

class Joystick extends EventDispatcher implements IJoystick {
	buttons: Set<string> = new Set();

	stick: Set<string> = new Set();

	eventMap: Map<string, string> = new Map<string, string>();

	keys: Set<string> = new Set();

	dispatch: Set<string> = new Set();

	stickDispatch: Set<string> = new Set();

	constructor() {
		super();
		// set up single action buttons
		const buttonEvents: Record<string, string> = Object.entries(
			KEYBINDS
		).reduce((prev, current) => {
			const [key, value] = current;
			// eslint-disable-next-line no-param-reassign
			prev[value] = `${keyboardPrefix}:${key}`;
			return prev;
		}, {} as Record<string, string>);

		// set up continously-firing joystick
		const stickEvents: Record<string, string> = Object.entries(JOYSTICK).reduce(
			(prev, current) => {
				const [key, value] = current;
				// eslint-disable-next-line no-param-reassign
				prev[value] = `${joystickPrefix}:${key}`;
				return prev;
			},
			{} as Record<string, string>
		);

		this.buttons = new Set(Object.values(KEYBINDS));
		this.stick = new Set(Object.values(JOYSTICK));
		this.eventMap = new Map(
			Object.entries({ ...buttonEvents, ...stickEvents })
		);
		// this.keys = new Set();
		// this.dispatch = new Set();
		// this.stickDispatch = new Set();

		Joystick.initialize(this);

		window.addEventListener('keydown', this.setKey.bind(this));
		window.addEventListener('keyup', this.resetKey.bind(this));
	}

	setKey(e: KeyboardEvent): void {
		const { key } = e;
		const isStickMovement = this.stick.has(key);
		const event = this.eventMap.get(key);

		if (event) {
			e.preventDefault(); // todo: should check what this does to window events
			const alreadyPressed = this.keys.has(key); // will be true when a key is held down

			if (!alreadyPressed) {
				this.keys.add(key);

				if (isStickMovement) {
					this.stickDispatch.add(event);
				} else {
					this.dispatch.add(event);
				}
			}
		}
	}

	resetKey(e: KeyboardEvent): void {
		const { key } = e;
		const isStickMovement = this.stick.has(key);
		const event = this.eventMap.get(key);

		if (event) {
			this.keys.delete(key);
			// resets continuously firing keys (left/right)
			if (isStickMovement) {
				this.stickDispatch.delete(event);
			}
		}
	}

	read(): void {
		// read stick
		this.stickDispatch.forEach((event) => {
			this.dispatchEvent(event);
		});

		// read button queue
		this.dispatch.forEach((event) => {
			this.dispatchEvent(event);
		});

		// clear single action buttons
		// todo: delete from set within loop?
		this.dispatch.clear();
	}
}

export default Joystick;

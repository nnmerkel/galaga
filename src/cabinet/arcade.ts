import {
	VERSION,
	DEBUG,
	TIMING,
	DEFAULT_HEIGHT,
	DEFAULT_WIDTH,
} from '~/constants';
import Game from '~/game';
import ScreenProvider from '~/cabinet/screenProvider';
import Logger from '~/game/systems/logger';
import Clock from '~/game/systems/clock';
import { ArcadeMode, CabinetTypes, Rank } from '~/enums';
import CreditManager from './creditManager';
import Joystick from './joystick';
import type {
	Dimensions,
	Cabinet,
	IArcade,
	ICreditManager,
	IJoystick,
	IGame,
	IArcadeOptions,
} from '~/types';
import type { TickerEvent } from 'easeljs';
// import Theme from './static/theme.json';

// original game dimensions
const dimensions: { [key in CabinetTypes]: Dimensions } = {
	UPRIGHT: {
		width: DEFAULT_WIDTH,
		height: DEFAULT_HEIGHT,
	},
	MINI: {
		width: 0, // todo
		height: 0, // todo
	},
	TABLE: {
		width: 0, // todo
		height: 0, // todo
	},
};

// sprites are generally 16 x 16 but can be other sizes as well
// see assets list for more info
const tile: Dimensions = {
	width: 16,
	height: 16,
};

// Arcade is the interface between the game and the browser
class Arcade implements IArcade {
	version: string = VERSION;

	// cabinet contains info about the original arcade game specs
	cabinet: Cabinet;

	// internal arcade state
	mode: ArcadeMode = ArcadeMode.selftest;

	// the actual game logic and resources
	game: IGame;

	creditManager: ICreditManager;

	joystick: IJoystick;

	logger = new Logger('Arcade');

	options: Required<IArcadeOptions> = {
		playerCount: 1,
		playerLives: 3,
		name: 'default-arcade',
		width: 480,
		root: null,
		rank: Rank.A,
		creditsForPlay: 2,
	};

	constructor(options: IArcadeOptions) {
		Object.assign(this.options, options);

		this.cabinet = {
			type: CabinetTypes.UPRIGHT,
			...dimensions[CabinetTypes.UPRIGHT],
			tile,
		};

		// make this 'global' for use across multiple components
		const { width, height } = ScreenProvider(
			ScreenProvider().config(this.options.width, this.cabinet)
		);

		// ancillary subsystems
		this.creditManager = new CreditManager(this.options.creditsForPlay);
		this.joystick = new Joystick();

		// attach to DOM
		const canvas = document.createElement('canvas');
		canvas.setAttribute('width', `${width}`);
		canvas.setAttribute('height', `${height}`);

		// todo: disable stage settings and listeners for events/features we dont need
		// EaselJS container
		(this.options.root ?? window.document.body).appendChild(canvas);
		this.game = new Game({ name: 'Galaga', canvas });
	}

	init(): void {
		// todo: remove listener on game start
		this.joystick.on('button:start', this.handleButtonStart, this);

		// setup debugging joystick events
		// actual game events are handled by the game
		if (DEBUG) {
			// this.joystick.on('button:a', this.handleButtonA, this);
			// this.joystick.on('button:b', this.handleButtonB, this);
			// this.joystick.on('button:select', this.handleButtonSelect, this);
			// this.joystick.on('joystick:up', this.handleButtonUp, this);
			// this.joystick.on('joystick:down', this.handleButtonDown, this);
			// this.joystick.on('joystick:left', this.handleButtonLeft, this);
			// this.joystick.on('joystick:right', this.handleButtonRight, this);
			this.joystick.on('button:print', this.printScreen, this);
		}

		this.game.preload().then(() => {
			this.game.init();
			Clock.on('tick', this.run, this);
			this.setMode(ArcadeMode.attract);
		});
	}

	// keeping for archival purposes
	// preload(callback) {
	//   const loaded = [];
	//   const assetList = Assets;

	//   assetList.forEach((asset, i) => {
	//     let item;
	//     let reader;

	//     if (asset.type === 'image') {
	//       // sprites, logos, etc.
	//       item = new Image();
	//       item.onload = function () {
	//         loaded.push({ id: asset.name, body: this });
	//         if (loaded.length === assetList.length) callback(loaded);
	//       };

	//       item.src = asset.src;
	//     } else if (asset.type === 'path') {
	//       // paths need loaded and parsed for easy calculations
	//       fetch(asset.src).then((response) => response.text()).then((data) => {
	//         const parser = new DOMParser();
	//         const svgDoc = parser.parseFromString(data, 'image/svg+xml');
	//         const pathData = svgDoc.getElementsByTagName('path')[0].getAttribute('d');

	//         loaded.push({ id: asset.name, body: pathData });
	//         if (loaded.length === assetList.length) callback(loaded);
	//       });
	//     }
	//   });
	// }

	// arcade clock
	run(e: TickerEvent): void {
		// this.logger.debug(Clock.getMeasuredTickTime(1)); // default: climbs to 160-170, raf: 150-160, synched: 140-150
		// this.logger.debug(Clock.getMeasuredFPS()); // default: levels off around 30-40, raf: 60, synched: 60
		e.paused = this.game.paused;

		if (!e.paused) {
			this.game.update(e); // must pass tick event for spritesheet framerate to work
		}

		this.joystick.read();
	}

	// BEGIN KEY HANDLERS
	// handleButtonA(): void {
	// 	console.log('button a');
	// }

	// handleButtonB(): void {
	// 	console.log('button b');
	// }

	handleButtonStart(): void {
		if (this.mode === ArcadeMode.attract) {
			// was idle, now awakening
			this.setMode(ArcadeMode.ready);
		} else if (this.mode === ArcadeMode.ready) {
			// was awaiting input, now starting a game
			this.setMode(ArcadeMode.play);
		}
	}

	// handleButtonSelect(): void {
	// 	console.log('button select');
	// }

	// handleButtonUp(): void {
	// 	console.log('button up');
	// }

	// handleButtonDown(): void {
	// 	console.log('button down');
	// }

	// handleButtonLeft(): void {
	// 	// console.log('button left');
	// }

	// handleButtonRight(): void {
	// 	// console.log('button right');
	// }

	setMode(mode: ArcadeMode): void {
		const validMode = Object.values(ArcadeMode).includes(mode);

		if (mode == null) {
			this.logger.debug('[setMode]: undefined mode');
			return;
		}

		if (mode === this.mode) {
			this.logger.debug('[setMode]: already set');
			return;
		}

		if (!validMode) {
			this.logger.debug('[setMode]: not a recognized mode');
			return;
		}

		this.mode = mode;
		this.logger.debug(`[setMode]: mode change: ${mode}`);

		switch (mode) {
			case ArcadeMode.selftest:
				this.runSelfTestMode();
				break;
			case ArcadeMode.attract:
				this.runAttractMode();
				break;
			case ArcadeMode.ready:
				this.runReadyMode();
				break;
			case ArcadeMode.play:
				this.runPlayMode();
				break;
			case ArcadeMode.highscore:
				this.runHighScoreMode();
				break;
			default:
				this.logger.debug(
					'[setMode]: valid mode but mismatched case condition'
				);
				this.resetMode();
		}
	}

	resetMode(): void {
		this.setMode(ArcadeMode.attract);
	}

	printScreen(): void {
		const i = new Image();

		i.onload = () => {
			document.body.appendChild(i);
		};

		i.src = (this.game.graphics.canvas as HTMLCanvasElement).toDataURL();
	}

	// BEGIN GAME STATE HANDLING FUNCTIONS
	// these functions are called one time per state change
	runSelfTestMode(): void {
		this.logger.debug('[runSelfTestMode]: running self test');
		this.creditManager.clear();
		// play explosion sound
		// upside-down display for 2 seconds
		// pressing 1P and 2P buttons at same time will keep display flipped until released
		// display config options
		// RAM
		// ROM
		// cabinet type
		// sound menu
		// 1st bonus
		// 2nd bonus
		// AND EVERY bonus
		this.logger.debug('self test', this);

		// this mode "soft ends", meaning it displays a cross-hatch pattern when entering
		// another mode from self-test mode. cross hatches stay on screen for 2 seconds
		// unless you press both 1P and 2P buttons at the same time
	}

	/*
	 * 1. The Attract Mode starts:
	 * - Just after power has been turned on to the game. (Self-Test switch is in the "OFF" position.)
	 * - After a Self-Test has been completed. (Performing a Self-Test sets the credits in the games memory to zero "0".)
	 * - After a play has been finished, the score was not high enough to put the game into the High Score/Initial mode, and there are no more credits left in the games memory.
	 * - After the High Score/Initial mode when there are no more credits left in its memory.
	 */
	runAttractMode(): void {
		// this.game.cycleAttractViews();
		// this.joystick.on('button:a', (e) => {
		//   this.setMode(modes.ready);
		// }, null, true);
		// this.display.render('attract');

		// todo: not sure i like this timing method, i need to better define what attract mode does
		setTimeout(() => {
			if (this.mode === ArcadeMode.attract) {
				this.game.startDemo();
			}
		}, TIMING);
	}

	/*
	 * 1. The Ready-To-Play mode starts when enough coins have been accepted for a 1 or a 2 player game
	 * 2. The Ready-To-Play mode ends when either the "1 Player" or "2 Player" push button is pressed.
	 */
	runReadyMode(): void {
		let timeElapsed = 0;

		const watchInput = () => {
			timeElapsed = 0;
			this.options.playerCount = (this.options.playerCount % 2) + 1;
		};

		const downKey = this.joystick.on('joystick:down', watchInput);
		const upKey = this.joystick.on('joystick:up', watchInput);

		this.game.endDemo();

		const activityMonitor = setInterval(() => {
			if (timeElapsed >= TIMING || this.mode !== ArcadeMode.ready) {
				this.joystick.off('joystick:down', downKey);
				this.joystick.off('joystick:up', upKey);

				// this.display.clear();
				clearInterval(activityMonitor);

				// todo: setting mode inside a runMode function doesnt make sense?
				// should probably check this inside activityMonitor
				if (this.mode !== ArcadeMode.play) {
					this.resetMode();
				}
			} else {
				timeElapsed += 1 * 1000;
			}
		}, 1000);
	}

	runPlayMode(): void {
		// set up hardware listeners
		const delegate = (e: Event) => {
			this.game.dispatchEvent(e);
		};

		// player fire
		this.joystick.on('button:a', (e: Event) => delegate(e));

		// player strafe
		this.joystick.on('joystick:left', (e: Event) => delegate(e));
		this.joystick.on('joystick:right', (e: Event) => delegate(e));

		this.game.start({
			playerCount: this.options.playerCount,
			playerLives: this.options.playerLives,
		});
	}

	runHighScoreMode(): void {
		this.game.enterHighScoreMode();
	}
}

export default Arcade;

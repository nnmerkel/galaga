/* eslint-disable @typescript-eslint/no-var-requires */
// node js parses this file so we need to use require()
const path = require('path');
const { DefinePlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const dotenv = require('dotenv');

const envFile = dotenv.config({
	path: path.resolve(__dirname, '.', '.env'),
}).parsed;

// console.log('hey: ', envFile, process.env, __dirname);

const config = {
	// watch: true,
	mode: process.env.NODE_ENV || 'development',
	entry: path.resolve(__dirname, 'build', 'index.js'),
	output: {
		filename: 'arcade.js',
		path: path.resolve(__dirname, 'dist'),
		clean: true,
	},
	resolve: {
		alias: {
			'~': path.resolve(__dirname, 'src/'),
		},
		extensions: ['.ts', '.tsx', '.js'],
	},
	devServer: {
		// open: true,
		allowedHosts: [envFile.ALLOWED_HOSTS || 'auto'],
		client: {
			logging: 'verbose',
			progress: true,
			overlay: {
				warnings: false,
				errors: true,
			},
		},
		hot: 'only',
		// host: '0.0.0.0',
		port: 3000,
		// historyApiFallback: true,
		server: {
			type: 'http', // https
			options: {},
		},
		static: {
			directory: path.join(__dirname, 'dist'),
		},
		watchFiles: [`${path.join(__dirname, 'build')}`],
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Galaga II',
			template: path.resolve(__dirname, 'src', 'index.html'),
		}),
		new DefinePlugin({
			'process.env': JSON.stringify(envFile),
		}),
	],
	module: {
		rules: [
			{
				test: /\.tsx?$/i,
				loader: 'ts-loader',
			},
			{
				test: /\.html$/i,
				// type: 'asset/source',
				loader: 'html-loader',
			},
			{
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(png|svg|jpe?g|gif|mp3|ttf)$/i,
				type: 'asset/resource',
				// loader: 'file-loader',
				// options: {
				//   name: '[path][name].[ext]',
				// },
			},
		],
	},
};

module.exports = config;

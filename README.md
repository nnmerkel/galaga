# Galaga

A (questionable) implemenetation of the classic Namco&copy; 1981 arcade game "Galaga" for the web.

This project is built in Typescript for all main browsers. Sprites and paths are SVG. All resources have been remade by hand to fit this build.

## Starting A Game Locally

First `yarn install`, then `yarn compile` to start the Typescript compiler. Run `yarn serve` to run a development server at [http://localhost:3000](http://localhost:3000)

## Available Scripts

- "yarn format:check": runs eslint. Does not write any changes.

- "yarn compile": compiles the source files into JS in a directory called "build" mirroring the structure of "src".

- "yarn serve": spins up a development server using Webpack. Bundles the compiled JS files into a single file named "arcade.js" that is used to run the game. All bundled files are moved into and served from "dist".

## Architecture

Galaga is written according to the official cabinet owner's manual. Whether or not this was a good idea, only time will tell.

### Arcade

The main global object is exposed to the `Window` as `arcade` (`window.arcade`). This superstructure represents the "hardware" side of the arcade like the coin slot, display, control interface, game program, and circuit board switch options. In JavascriptLand, the arcade serves as a mechanism to embed the game into the browser in a reliable and consistent way.

#### Arcade options

- `width: number` : set the arcade canvas width. The game will attempt to scale as cleanly as possible to any width, but for best results use a multiple of 240. (default: 480)
- `playerCount: number` : currently only supports 1 or 2 players. (default: 1)
- `playerLives: number` : set how many lives players start with. (default: 3)
- `creditsForPlay: number` : not yet implemented; set how many "coins" a user needs to play. (default: 2)
- `rank: Rank` : sets the difficulty level of the game. Rank is an enumerable from 'A' to 'D' where 'A' is the least difficult and 'D' is the most difficult. (default: Rank.A)

#### Arcade properties

- `mode: number` : internal arcade state. Not related to game state.
- `cabinet: Cabinet` : configuration information for scaling the game assets to the provided display width.

#### Arcade.creditManager

`CreditManager` is the implementation of a physical arcade coin slot. It is an artifact of the physical arcade cabinet game. I built it to be thorough but it does not do anything yet.

#### Arcade.joystick

Main class for the arcade control interface. Keybinds are fully customizable with your own build. Default controls:

- Directional
  - Up: ArrowUp
  - Down: ArrowDown
  - Left: ArrowLeft
  - Right: ArrowRight
- Buttons
  - Start: Z
  - Select: X
  - A: Space
  - B: C
  - Print: P

#### Arcade.root

DOM element wrapping the arcade canvas. If not provided, the canvas will be appended to `document.body`.

#### Arcade.game

Mount for the actual game code. Detailed outline below.

### Game

#### Game options

- `playerCount: number` : inherited from `Arcade`.
- `playerLives: number` : inherited from `Arcade`.
- `missilesOnScreenLimit: number` : defaults to 2 like the original game. only applies to player's missiles. (default: 2)

#### Game properties
